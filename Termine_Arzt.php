<html>
  <head>
    <link rel="stylesheet" href="login.css">
    <title>Login</title>
    <script language="javascript" type="text/javascript" src="javascript.js"></script>
  </head>
  <body>
  <?php
              session_start();
              $servername = "localhost";
              $user = "root";
              $pw = "";
              $db = "antau";
  
      
  
  
              $con = new mysqli($servername,$user,$pw,$db);

              if($con->connect_error){
                  echo("Verbindung fehlgeschlagen".$con->connect_error);
              }
              $search_user = $con->prepare("SELECT * FROM aerzte WHERE ID = ?");
              $search_user->bind_param('i',$_SESSION['user']);
              $search_user->execute();
              $search_res = $search_user->get_result();

              if(isset($_POST['abmelden'])){
                session_destroy();
                header('location: Startseite.php');
              }


    ?>
    <a href="Startseite.php"><img id="logo" src="Antau.png"></a>
    <?php
    $sql = "SELECT * FROM termine";
    if($erg = $con->query($sql)){
      while($datensatz = $erg->fetch_object()){
        $daten[] = $datensatz;
      }
    }
    ?>

    <div class="wrapper fadeInDown">
    <table id="meineTabelle" data-role="table" class="ui-responsive"
           data-mode="columntoggle" data-column-btn-text="Spalten" >
      <thead>
        <tr>
          <th data-priority="5">ID</th>
          <th>Datum</th>
          <th data-priority="1">Uhrzeit</th>
          <th data-priority="2">Patient</th>
          <th data-priority="3">Grund</th>
          <th data-priority="4">Raum</th>
        </tr>
      </thead>
      <tbody>
    <?php
    foreach ($daten as $inhalt) {
    ?>
        <tr>
            <td>
                <?php echo $inhalt->ID; ?>
            </td>
            <td>
                <?php echo $inhalt->Datum; ?>
            </td>
            <td>
                <?php echo $inhalt->Uhrzeit; ?>
            </td>
            <td>
                <?php echo $inhalt->Grund; ?>
            </td>
            <td>
                <?php echo $inhalt->Raum; ?>
            </td>           
      </tr>
    <?php
    }
    ?>
      </tbody>
    </table>

  </body>
</html>
