<!DOCTYPE html>
<html>

  <head>
    <link rel="stylesheet" href="login.css">
    <title>Registrierung</title>
    <script language="javascript" type="text/javascript" src="javascript.js"></script>
  </head>
  <body>
    <a href="Startseite.php"><img id="logo" src="Antau.png"></a>
    <div class="wrapper fadeInDown">
    <div id="formContent">

      <h2 class="active fadeIn fourth">Registrieren </h2>

      <form action="" method="post">
        <input type="text" id="Vorname" class="fadeIn second" name="vorname" placeholder="Vorname">
        <input type="text" id="Nachname" class="fadeIn third" name="nachname" placeholder="Nachname">
        <input type="date" id="Geburtstag" class="fadeIn third" name="geburtstag" placeholder="Geburtstag">
        <input type="text" id="Adresse" class="fadeIn third" name="adresse" placeholder="Adresse">
        <input type="text" id="Krankenkasse" class="fadeIn third" name="krankenkasse" placeholder="Krankenkasse">
        <input type="number" id="Krankenkassennummer" class="fadeIn third" name="krankenkassennummer" placeholder="Krankenkassennummer">
        <input type="text" id="Versicherungsart" class="fadeIn third" name="versicherungsart" placeholder="Versicherungsart">
        <input type="number" id="Telefonnummer" class="fadeIn third" name="telefonnummer" placeholder="Handynummer">
        <input type="text" id="E-Mail" class="fadeIn third" name="email" placeholder="E-Mail">
        <input type="Password" id="password" class="fadeIn third" name="password" placeholder="Password">

        <a href="Einloggen.php"><input type="submit" class="fadeIn fourth" name="absenden" value="Registrieren"></a>
      
    </form>
      

      

    </div>
  </div>
  <?php

            $servername = "localhost";
            $user = "root";
            $pw = "";
            $db = "antau";

    


            $con = new mysqli($servername,$user,$pw,$db);

            if($con->connect_error){
                echo("Verbindung fehlgeschlagen".$con->connect_error);
            }
            if(isset($_POST['absenden'])){
              $vorname = $_POST['vorname'];
              $nachname = $_POST['nachname'];
              $geburtstag = $_POST['geburtstag'];
              $adresse = $_POST['adresse'];
              $krankenkasse = $_POST['krankenkasse'];
              $krankenkassennummer = $_POST['krankenkassennummer'];
              $versicherungsart = $_POST['versicherungsart'];
              $telefonnummer = $_POST['telefonnummer'];
              $email = $_POST['email'];
              $password = $_POST['password'];
              $password_hash = password_hash($password, PASSWORD_DEFAULT);
              
              $insert =$con->prepare("INSERT INTO patienten (`Vorname`, `Nachname`, `Geburtstag`, `Adresse`, `Krankenkasse`, `Krankenkassennummer`, `Versicherungsart`, `Telefonnummer`, `EMail`, `Passwort`) VALUES (?,?,?,?,?,?,?,?,?,?)");
              $insert->bind_param('ssdssisiss',$vorname,$nachname,$geburtstag,$adresse,$krankenkasse,$krankenkassennummer,$versicherungsart,$telefonnummer,$email,$password_hash);
              $insert->execute();
              header("Location: Einloggen.php");
            }           
            $con->close();
            
        ?>
  </body>
</html>
