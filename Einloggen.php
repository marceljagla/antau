<html>
  <head>
    <link rel="stylesheet" href="login.css">
    <title>Login</title>
    <script language="javascript" type="text/javascript" src="javascript.js"></script>
  </head>
  <body>    
    <?php
              $servername = "localhost";
              $user = "root";
              $pw = "";
              $db = "antau";
  
      
  
  
              $con = new mysqli($servername,$user,$pw,$db);
  
              if($con->connect_error){
                  echo("Verbindung fehlgeschlagen".$con->connect_error);
              }
        if(isset($_POST['absenden'])){
        $email = $_POST['email'];
        $passwort = $_POST['passwort'];
        $search_patient = $con->prepare("SELECT ID FROM patienten WHERE EMail = ? AND Passwort =?");
        $search_patient->bind_param('ss',$email,$passwort);
        $search_patient->execute();
        $count = $search_patient->get_result();
        
        //header("Location: Startseite_Eingelogt.php");
        if($count->num_rows==1){
            $row = $count->fetch_object();
                $_SESSION['user'] = $row->ID;
                header("Location: Startseite_Eingelogt.php");
               }
               elseif($count->num_rows==0){
                $search_arzt = $con->prepare("SELECT ID FROM aerzte WHERE Email = ? AND Passwort =?");
                $search_arzt->bind_param('ss',$email,$passwort);
                $search_arzt->execute();
                $count = $search_arzt->get_result();
                if($count->num_rows==1){
                    $row = $count->fetch_object();
                    $_SESSION['user'] = $row->ID;
                    header("Location: Startseite_Eingelogt.php");
                }
               }

        }
    ?>
    <a href="Startseite.php"><img id="logo" src="Antau.png"></a>
    <div class="wrapper fadeInDown">
    <div id="formContent">
      <!-- Tabs Titles -->
      <h2 class="active"> Anmelden </h2>
      <a href="Registrieren_als.php"><h2 class="inactive underlineHover">Registrieren </h2></a>

      <form action="" method="post">
        <input type="text" id="email" class="fadeIn second" name="email" placeholder="E-Mail">
        <input type="password" id="passwort" class="fadeIn third" name="passwort" placeholder="password">

        <input type="submit" id="abgeschickt" class="fadeIn fourth" name="absenden" value="Log in" >
      </form>
      

      <!-- Remind Passowrd -->
      <div id="formFooter">
        <a class="underlineHover" href="#">Forgot Password?</a>
      </div>

    </div>
  </div>

  </body>
</html>
