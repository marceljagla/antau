<html>
  <head>
    <link rel="stylesheet" href="login.css">
    <title>Termin</title>
    <script language="javascript" type="text/javascript" src="javascript.js"></script>
  </head>
  <body>
    <a href="Startseite.php"><img id="logo" src="Antau.png"></a>
    <div class="wrapper fadeInDown">
    <div id="formContent">
      <a href=""><h2 class="active">Was ist ihr Termin grund?</h2></a><br>

      <hr>
      <form action="" method="post">
        <input type="text" id="Vorname" class="fadeIn second" name="patientenID" placeholder="PatientenID">
        <input type="date" id="Geburtstag" class="fadeIn third" name="datum" placeholder="Datum"><br><br>
        <input type="time" id="Adresse" class="fadeIn third" name="uhrzeit" min="09:00" max="17:00" placeholder="Uhrzeit"><br>
        <small>Praxis Zeiten von 09:00-17:00 Uhr</small>
        <input type="text" id="Krankenkasse" class="fadeIn third" name="aerzteID" placeholder="aerzteID">
        <textarea id="text" class="fadeIn third" name="grund" cols="35" rows="4" placeholder="Beschwerden"></textarea>

        <a><input type="submit" id="abgeschickt" class="fadeIn fourth" name="absenden" value="Termin Buchen" ></a>
      
    </form>
    </div>
    </div>
        <?php

            $servername = "localhost";
            $user = "root";
            $pw = "";
            $db = "antau";

    


            $con = new mysqli($servername,$user,$pw,$db);

            if($con->connect_error){
                echo("Verbindung fehlgeschlagen".$con->connect_error);
            }
            if(isset($_POST['absenden'])){
              $patientenID = $_POST['patientenID'];
              $datum = $_POST['datum'];
              $uhrzeit = $_POST['uhrzeit'];
              $aerzteID = $_POST['aerzteID'];
              $grund = $_POST['grund'];
              


              $insert =$con->prepare("INSERT INTO termine (`Uhrzeit`, `Datum`, `PatientenID`, `AerzteID`, `Grund`) VALUES (?,?,?,?,?)");
              $insert->bind_param('idiis',$uhrzeit,$datum,$patientenID,$aerzteID,$grund);
              $insert->execute();
              header("Location: Startseite.php");
            }           
            
        ?>
  </body>
</html>
